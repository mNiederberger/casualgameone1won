﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

    Vector3 targetPosition;
    Vector3 startPosition;
    private GameObject[] objects;
    private GameObject hostage;
    public bool hitHostage = false;
    private GameObject villain;
    public bool hitVillain = false;
    private GameObject player;
    public bool hitPlayer = false;

    enum DIRECTION
    { North,
      South,
      East,
      West,
      NorthEast,
      NorthWest,
      SouthEast,
      SouthWest,};
    DIRECTION direction;
    DIRECTION startDirection;

    // Use this for initialization
    void Start () {
        hostage = GameObject.FindGameObjectWithTag("hostage");
        villain = GameObject.FindGameObjectWithTag("villain");
        objects = GameObject.FindGameObjectsWithTag("moveable");
        player = GameObject.FindGameObjectWithTag("Player");

        Debug.Log("hostage pos"+hostage.transform.position);



        //Choose start direction
        if (hostage.transform.position.z > this.transform.position.z)
        {
            startDirection = DIRECTION.North;
        }
        else if (hostage.transform.position.z < this.transform.position.z)
        {
            startDirection = DIRECTION.South;
        }
        else if (hostage.transform.position.x > this.transform.position.x)
        {
            startDirection = DIRECTION.East;
        }
        else if (hostage.transform.position.x < this.transform.position.x)
        {
            startDirection = DIRECTION.West;
        }

        fire();

        Debug.Log("Direction: " + startDirection.ToString());
    }

    void fire()
    {
        hitHostage = false;
        hitVillain = false;
        hitPlayer = false;
        this.transform.position = new Vector3((int)villain.transform.position.x, 0, (int)villain.transform.position.z);
        startPosition = this.transform.position;
        targetPosition = startPosition;
        
        direction = startDirection;
   
        Debug.Log("Direction: " + direction.ToString());
    }
	
	// Update is called once per frame
	void Update ()
    {
        //Debug.Log("Bullet sees box at" + objects[0].transform.position);
        //Debug.Log("Bullet is headed to" + targetPosition + ", direction is" + direction);
        if (player.transform.position == startPosition || player.transform.position == targetPosition)
        {
            hitPlayer = true;
            Debug.Log("Player killed");
        }

    }

    public void move()
    {
        startPosition = this.transform.position;

        for (int i = 0; i < objects.Length; i++)
        {
            if (objects[i].transform.position.x == startPosition.x && objects[i].transform.position.z == startPosition.z)
            {
                Debug.Log("Object in the way");
                if (objects[i].name == "NWPipe")
                {
                    if (direction == DIRECTION.North || direction == DIRECTION.West)
                    {
                        Debug.Log("Hit pipe wrong");
                        fire();
                    }
                    else if (direction == DIRECTION.South)
                    {
                        direction = DIRECTION.West;
                        targetPosition = new Vector3(startPosition.x - 1, startPosition.y, startPosition.z);
                    }
                    else if (direction == DIRECTION.East)
                    {
                        direction = DIRECTION.North;
                        targetPosition = new Vector3(startPosition.x, startPosition.y, startPosition.z + 1);
                    }
                }
                else if (objects[i].name == "NEPipe")
                {
                    if (direction == DIRECTION.North || direction == DIRECTION.East)
                    {
                        Debug.Log("Hit pipe wrong");
                        fire();
                    }
                    else if (direction == DIRECTION.South)
                    {
                        direction = DIRECTION.East;
                        targetPosition = new Vector3(startPosition.x + 1, startPosition.y, startPosition.z);
                    }
                    else if (direction == DIRECTION.West)
                    {
                        direction = DIRECTION.North;
                        targetPosition = new Vector3(startPosition.x, startPosition.y, startPosition.z + 1);
                    }
                }
                else if (objects[i].name == "SWPipe")
                {
                    if (direction == DIRECTION.South || direction == DIRECTION.West)
                    {
                        Debug.Log("Hit pipe wrong");
                        fire();
                    }
                    else if (direction == DIRECTION.North)
                    {
                        direction = DIRECTION.West;
                        targetPosition = new Vector3(startPosition.x - 1, startPosition.y, startPosition.z);
                    }
                    else if (direction == DIRECTION.East)
                    {
                        direction = DIRECTION.South;
                        targetPosition = new Vector3(startPosition.x, startPosition.y, startPosition.z - 1);
                    }
                }
                else if (objects[i].name == "SEPipe")
                {
                    if (direction == DIRECTION.South || direction == DIRECTION.East)
                    {
                        Debug.Log("Hit pipe wrong");
                        fire();
                    }
                    else if (direction == DIRECTION.North)
                    {
                        direction = DIRECTION.East;
                        targetPosition = new Vector3(startPosition.x + 1, startPosition.y, startPosition.z);
                    }
                    else if (direction == DIRECTION.West)
                    {
                        direction = DIRECTION.South;
                        targetPosition = new Vector3(startPosition.x, startPosition.y, startPosition.z - 1);
                    }
                }
            }
        }

        if (this.direction == DIRECTION.North)
        {
            targetPosition = new Vector3(startPosition.x, startPosition.y, startPosition.z + 1);
        }
        else if (this.direction == DIRECTION.South)
        {
            targetPosition = new Vector3(startPosition.x, startPosition.y, startPosition.z - 1);
        }
        else if (this.direction == DIRECTION.East)
        {
            targetPosition = new Vector3(startPosition.x + 1, startPosition.y, startPosition.z);
        }
        else if (this.direction == DIRECTION.West)
        {
            targetPosition = new Vector3(startPosition.x - 1, startPosition.y, startPosition.z);
        }
        
        if (hostage.transform.position == targetPosition)
        {
            hitHostage = true;
            Debug.Log("Hostage killed");
        }
        else if (villain.transform.position == targetPosition)
        {
            hitVillain = true;
            Debug.Log("Villain killed");
        }

        
        if (targetPosition.x > 4.0f || targetPosition.x < 0.0f || targetPosition.z < -6.0f || targetPosition.z > 0.0f)
        {
            Debug.Log("Off screen");
            fire();
        }

        Debug.Log("Direction: " + direction.ToString());

        /* Old bullet react code- unrealistic looking
        for (int i =0; i < objects.Length; i++)
        {
            if (objects[i].transform.position.x == targetPosition.x && objects[i].transform.position.z == targetPosition.z)
            {
                Debug.Log("Object in the way");
                if (objects[i].name == "NWPipe")
                {
                    if (direction == DIRECTION.North || direction == DIRECTION.West)
                    {

                    }
                    else if (direction == DIRECTION.South)
                    {
                        direction = DIRECTION.West;
                        targetPosition.x--;
                    }
                    else if (direction == DIRECTION.East)
                    {
                        direction = DIRECTION.North;
                        targetPosition.z++;
                    }
                }
                else if(objects[i].name == "NEPipe")
                {
                    if (direction == DIRECTION.North || direction == DIRECTION.East)
                    {

                    }
                    else if (direction == DIRECTION.South)
                    {
                        direction = DIRECTION.East;
                        targetPosition.x++;
                    }
                    else if (direction == DIRECTION.West)
                    {
                        direction = DIRECTION.North;
                        targetPosition.z++;
                    }
                }
                else if (objects[i].name == "SWPipe")
                {
                    if (direction == DIRECTION.South || direction == DIRECTION.West)
                    {

                    }
                    else if (direction == DIRECTION.North)
                    {
                        direction = DIRECTION.West;
                        targetPosition.x--;
                    }
                    else if (direction == DIRECTION.East)
                    {
                        direction = DIRECTION.South;
                        targetPosition.z--;
                    }
                }
                else if (objects[i].name == "SEPipe")
                {
                    if (direction == DIRECTION.South || direction == DIRECTION.East)
                    {
                        
                    }
                    else if (direction == DIRECTION.North)
                    {
                        direction = DIRECTION.East;
                        targetPosition.x++;
                    }
                    else if (direction == DIRECTION.West)
                    {
                        direction = DIRECTION.North;
                        targetPosition.z--;
                    }
                }
            }
        }*/
    }

    public Vector3 getTargetPos()
    {
        return targetPosition;
    }
    public Vector3 getStartPos()
    {
        return startPosition;
    }

    public void setTargetPos(Vector3 targpos)
    {
        targetPosition = targpos;
    }
    public void setStartPos(Vector3 startpos)
    {
        startPosition = startpos;
    }
}
