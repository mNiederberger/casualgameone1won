﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

    Vector3 targetPosition;
    Vector3 startPosition;
    
    // Use this for initialization
    void Start () {
        this.transform.position = new Vector3((int)this.transform.position.x, 0, (int)this.transform.position.z);
    }
	
	// Update is called once per frame
	void Update () {
        Vector3 moveDir = Vector3.zero;

    }

    public Vector3 getTargetPos()
    {
        return targetPosition;
    }
    public Vector3 getStartPos()
    {
        return startPosition;
    }

    public void setTargetPos(Vector3 targpos)
    {
        targetPosition = targpos;
    }
    public void setStartPos(Vector3 startpos)
    {
        startPosition = startpos;
    }
}
