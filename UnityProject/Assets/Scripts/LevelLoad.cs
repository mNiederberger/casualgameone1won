﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class LevelLoad : MonoBehaviour
{
	[SerializeField]
	private string menu;
	bool isMenu = true;
	[SerializeField]
	private List<string> levelNames = new List<string>();
	private int currentLevelIndex = -1;

	//GameObject managerObject;
	private GameManager currentManager;

	// Use this for initialization
	void Start()
	{
		SceneManager.LoadScene(menu, LoadSceneMode.Additive);
	}

	// Update is called once per frame
	void Update()
	{
		if(currentManager == null && !isMenu)
		{
			currentManager = GameObject.FindObjectOfType<GameManager>();
		}
		

		//////HACK BUTTONS
		if (Input.GetKeyDown("j"))
		{
			LoadNextLevel();
		}
        if(Input.touchCount > 0 && isMenu)
        {
            LoadNextLevel();
        }
	}

	public void UnloadLevel()
	{
		if (!isMenu)
		{
			SceneManager.UnloadScene(levelNames[currentLevelIndex]);
		}
		else
		{
			SceneManager.UnloadScene(menu);
			isMenu = false;
		}
		currentManager = null;
	}

	public void LoadNextLevel()
	{
		UnloadLevel();

		++currentLevelIndex;
		if (currentLevelIndex >= levelNames.Count)
		{
			currentLevelIndex = 0;
		}

        Debug.Log(levelNames[currentLevelIndex]);
		SceneManager.LoadScene(levelNames[currentLevelIndex], LoadSceneMode.Additive);
	}

	public void ReloadLevel()
	{
		UnloadLevel();
		//Debug.Log(levelNames[currentLevelIndex]);
		SceneManager.LoadScene(levelNames[currentLevelIndex], LoadSceneMode.Additive);
	}
}
