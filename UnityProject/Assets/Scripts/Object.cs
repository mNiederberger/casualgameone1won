﻿using UnityEngine;
using System.Collections;

public class Object : MonoBehaviour {
    
    GameObject manager;
    GameObject player;
    GameManager gm;

    // Use this for initialization
    void Start () {
         manager = GameObject.Find("GameManager");
        player = GameObject.Find("Player");
         gm = manager.GetComponent<GameManager>();
    }
	
	// Update is called once per frame
	void Update () {
        this.transform.position = new Vector3(Mathf.Round(this.transform.position.x), this.transform.position.y, Mathf.Round(this.transform.position.z));
    }



    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "highlights")
        {
            //Debug.Log("colliding with object");
            float tempX = this.gameObject.transform.position.x - player.gameObject.transform.position.x;
            float tempZ = this.gameObject.transform.position.z - player.gameObject.transform.position.z;

            if (tempX == 1 && tempZ == 0)
            {
               // gm.cantMoveRight = true;
                Debug.Log("Cant right");
            }
           else if(tempX == -1 && tempZ == 0)
            {
                //gm.cantMoveLeft = true;
                Debug.Log("cant left");
            }
            else if(tempZ == 1 && tempX == 0)
            {
                //gm.cantMoveUp = true;
                Debug.Log("cant up");
            }
            else if(tempZ == -1 && tempX == 0)
            {
                //gm.cantMoveDown = true;
                Debug.Log("cant down");
            }
        }
    }

}
