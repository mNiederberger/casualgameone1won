﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    private GameObject playerObj;
    private Player player;
    private GameObject item;
    Vector3 itemTargetPosition;
    Vector3 itemStartPosition;
    

    [SerializeField]
    private GameObject bulletObj;
    private Bullet bullet;
    private Object[] objects;
    public GameObject[] highlighters;

    [SerializeField]
	private float moveTime;
	private float moveTimer = 0;

	[SerializeField]
	private float resetTime;
	private float resetTimer = 0;

	[SerializeField]
    private Canvas loseCanvas;

    [SerializeField]
    private Canvas winCanvas;
    private Text moveText;
    int moveAmount = 0;

	private GAMESTATE gameState;

	public enum GAMESTATE
	{
		WaitForInput,
		Moving,
		LevelComplete,
        LevelFailed,
	}

    private Vector2 startPos;

    private bool bulletMove = true;

    // Use this for initialization
    void Start()
	{
        player = playerObj.GetComponent<Player>();
        bullet = bulletObj.GetComponent<Bullet>();
        objects = GetComponents<Object>();
        highlighters = GameObject.FindGameObjectsWithTag("highlights");
        //player.transform.position = new Vector3((int)player.transform.position.x, 0, (int)player.transform.position.z);
        //bullet.transform.position = new Vector3((int)bullet.transform.position.x, 0, (int)bullet.transform.position.z);
        gameState = GAMESTATE.WaitForInput;
        // item.transform.position = new Vector3((int)item.transform.position.x, (int)item.transform.position.y, (int)item.transform.position.z);
        loseCanvas.gameObject.SetActive(false);
        winCanvas.gameObject.SetActive(false);
        moveText = winCanvas.GetComponentInChildren<Text>();
    }

	// Update is called once per frame
	void Update()
	{

		Vector3 moveDir = Vector3.zero;

		//State machine for the game
		switch(gameState)
		{
			case GAMESTATE.WaitForInput:
                {
					resetTimer = 0;

                    foreach (GameObject light in highlighters)
                    {
                        light.gameObject.SetActive(true);
                    }

                    if (Input.touchCount > 0)
                    {

                        Touch touch = Input.touches[0];

                        switch (touch.phase)
                        {
                            case TouchPhase.Began:
                                startPos = touch.position;
                                break;

                            case TouchPhase.Ended:
                                float swipDistVert = (new Vector3(0, touch.position.y, 0) - new Vector3(0, startPos.y, 0)).magnitude;
                                float swipDistHorz = (new Vector3(touch.position.x, 0, 0) - new Vector3(startPos.x, 0, 0)).magnitude;

                                if (swipDistVert > swipDistHorz)
                                {
                                    float swipeValue = Mathf.Sign(touch.position.y - startPos.y);
                                    if (swipeValue > 0)
                                    {
                                        gameState = GAMESTATE.Moving;
                                        moveDir = Vector3.forward;
                                        if (bulletMove == true)
                                        {
                                            bullet.move();
                                            bulletMove = false;
                                            Debug.Log("bullet moving ");
                                        }
                                        else
                                        {
                                            bulletMove = true;
                                        }
                                    }
                                    else if (swipeValue < 0)
                                    {
                                        gameState = GAMESTATE.Moving;
                                        moveDir = Vector3.back;
                                        if (bulletMove == true)
                                        {
                                            bullet.move();
                                            bulletMove = false;
                                            Debug.Log("bullet moving ");
                                        }
                                        else
                                        {
                                            bulletMove = true;
                                        }
                                    }
                                }
                                else if (swipDistHorz > swipDistVert)
                                {
                                    float swipeValue = Mathf.Sign(touch.position.x - startPos.x);
                                    if (swipeValue > 0)
                                    {
                                        gameState = GAMESTATE.Moving;
                                        moveDir = Vector3.right;
                                        if (bulletMove == true)
                                        {
                                            bullet.move();
                                            bulletMove = false;
                                            Debug.Log("bullet moving ");
                                        }
                                        else
                                        {
                                            bulletMove = true;
                                        }
                                    }
                                    else if (swipeValue < 0)
                                    {
                                        gameState = GAMESTATE.Moving;
                                        moveDir = Vector3.left;
                                        if (bulletMove == true)
                                        {
                                            bullet.move();
                                            bulletMove = false;
                                            Debug.Log("bullet moving ");
                                        }
                                        else
                                        {
                                            bulletMove = true;
                                        }
                                    }
                                }
                                break;
                        }
                    }

                    moveTimer = 0;
                    if (Input.GetAxis("Vertical") < 0)
                    {
                        gameState = GAMESTATE.Moving;
                        moveDir = Vector3.back;
                        if (bulletMove == true)
                        {
                            bullet.move();
                            bulletMove = false;
                            Debug.Log("bullet moving ");
                        }
                        else
                        {
                            bulletMove = true;
                        }
                    }
                    else if (Input.GetAxis("Vertical") > 0)
                    {
                        gameState = GAMESTATE.Moving;
                        moveDir = Vector3.forward;
                        if (bulletMove == true)
                        {
                            bullet.move();
                            bulletMove = false;
                            Debug.Log("bullet moving ");
                        }
                        else
                        {
                            bulletMove = true;
                        }
                    }
                    else if (Input.GetAxis("Horizontal") < 0)
                    {
                        gameState = GAMESTATE.Moving;
                        moveDir = Vector3.left;
                        if (bulletMove == true)
                        {
                            bullet.move();
                            bulletMove = false;
                            Debug.Log("bullet moving ");
                        }
                        else
                        {
                            bulletMove = true;
                        }
                    }
                    else if (Input.GetAxis("Horizontal") > 0)
                    {
                        gameState = GAMESTATE.Moving;
                        moveDir = Vector3.right;
                        if (bulletMove == true)
                        {
                            bullet.move();
                            bulletMove = false;
                            Debug.Log("bullet moving ");
                        }
                        else
                        {
                            bulletMove = true;
                        }
                    }

                player.setStartPos(player.transform.position);
                player.setTargetPos(player.transform.position + moveDir);
                //Debug.Log(Input.GetAxis("Horizontal"));
                //Debug.Log(Input.GetAxis("Vertical"));
                moveTimer = 0;
				if (Input.GetAxis("Vertical") < 0)
				{
                    moveAmount += 1;
                    gameState = GAMESTATE.Moving;
					moveDir = Vector3.back;
				}
				else if (Input.GetAxis("Vertical") > 0)
				{
                    moveAmount += 1;
                    gameState = GAMESTATE.Moving;
					moveDir = Vector3.forward;
				}
				else if (Input.GetAxis("Horizontal") < 0)
				{
                    moveAmount += 1;
                    gameState = GAMESTATE.Moving;
					moveDir = Vector3.left;
				}
				else if (Input.GetAxis("Horizontal") > 0)
				{
                    moveAmount += 1;
                    gameState = GAMESTATE.Moving;
					moveDir = Vector3.right;
				}

                if(bullet.hitHostage || bullet.hitPlayer)
                {
                    gameState = GAMESTATE.LevelFailed;
                }

                if(bullet.hitVillain)
                {
                    gameState = GAMESTATE.LevelComplete;
                }

				player.setStartPos(player.transform.position);
				player.setTargetPos(player.transform.position + moveDir);
					//Debug.Log(Input.GetAxis("Horizontal"));
					//Debug.Log(Input.GetAxis("Vertical"));


                    //Movement precheck
                    RaycastHit h1, h2;
				if (Physics.Raycast(playerObj.transform.position, moveDir, out h1, 1.0f) && Physics.Raycast(playerObj.transform.position + moveDir, moveDir, out h2, 1.0f))
				{
					Debug.Log("Can't move in direction " + moveDir);
						Debug.Log("hit at " + h1.distance);
						Debug.Log("hit2 at " + h2.distance);
						gameState = GAMESTATE.WaitForInput;
					moveDir = Vector3.zero;
					
				}
			}
			break;

			case GAMESTATE.Moving:
			{
                    foreach (GameObject light in highlighters)
				{
					light.gameObject.SetActive(false);
				}
				moveTimer += Time.deltaTime;

				float t = (-(moveTimer - moveTime) / moveTime);
				player.transform.position = Vector3.Lerp(player.getTargetPos(), player.getStartPos(), t);
                    if (bulletMove == false)
                    {
                        bullet.transform.position = Vector3.Lerp(bullet.getTargetPos(), bullet.getStartPos(), t);
                    }

				if (moveTimer >= moveTime)
				{
					player.transform.position = player.getTargetPos();
					bullet.transform.position = bullet.getTargetPos();
					gameState = GAMESTATE.WaitForInput;
				}

			}
			break;

            case GAMESTATE.LevelFailed:
            {
                    loseCanvas.gameObject.SetActive(true);

					resetTimer += Time.deltaTime;

					if(resetTimer > resetTime)
					{
						GameObject.Find("LevelLoadManager").GetComponent<LevelLoad>().ReloadLevel();
					}
            }
                break;

            case GAMESTATE.LevelComplete:
            {
                    winCanvas.gameObject.SetActive(true);

                    moveText.text = ("Completed in " + moveAmount + " moves");

					resetTimer += Time.deltaTime;

					if (resetTimer > resetTime)
					{
						GameObject.Find("LevelLoadManager").GetComponent<LevelLoad>().LoadNextLevel();
					}

				}
                break;
		}
		//Debug.Log(gameState
	}
}
